function ceklogin(){
    token = localStorage.getItem('token');
    if (token) {
        console.log('di home')
    } else{
        window.location.href= '/login'
        console.log('di login')
    }
}
ceklogin();

$('#logout').click(function(e){
    e.preventDefault();
    $.ajax({
        type : "POST",
        url : "http://localhost:3001/v2/logout",
        success : function(data){
            localStorage.token ="";
            window.location.href="/login";
        },
        error : function(err){
            console.log('gagal logout')
        }
    })
})

var cities;
var urlCityRaw = "https://demoapi.blood4life.id/v1/provinces/c/";
var urlCity;
var provinces;
var urlProv = "https://demoapi.blood4life.id/v1/provinces";

$('#Add').on('click', function(){
    $('#formSubmit').show();
    $('#viewData').hide();
    $('#formUpdate').hide();
});
$('#view').on('click', function(){
    $('#formSubmit').hide();
    $('#viewData').show();
    $('#formUpdate').hide();
});

$.getJSON(urlProv, function(json) {
  var data = json;
	for (i = 0; i < data.rajaongkir.results.length-1; i++) {
	 	provinces ='<option value="'+data.rajaongkir.results[i].province+'">'+ data.rajaongkir.results[i].province+'</option>';
	 	$('#formProv, #formprovUpdate').append(provinces);
	}
	$('#formProv, #formprovUpdate').on('change', function(e){
	 	$('#formKota, #formkotaUpdate').empty();
	 	var optionSelected = $("option:selected", this);
    	var valueSelected = this.value;
    	for(var j=0; j < data.rajaongkir.results.length-1;j++){
    		if (valueSelected == data.rajaongkir.results[j].province) {
    			urlCity = urlCityRaw + data.rajaongkir.results[j].province_id;
    			$.getJSON(urlCity, function(json){
    				var data = json;
    				for (var k = 0; k < data.rajaongkir.results.length-1; k++) {
    					cities = '<option value="'+data.rajaongkir.results[k].type+' '+data.rajaongkir.results[k].city_name+'">'+data.rajaongkir.results[k].type+' '+data.rajaongkir.results[k].city_name+'</option>';
            			$('#formKota, #formkotaUpdate').append(cities);
    				}
    			});
    		}
    	}
	 });
});

var idEdit;

function tampilkanFormUpdate(id){
    console.log(id);
    $('#formUpdate').show();
    $('#viewData').hide();
    $('#formSubmit').hide();
    $.getJSON('http://localhost:3001/v1/rumahsakit/'+id, function(json){
        var data = json.result;
        $('#name').val(data.name);
        $('#alamat').val(data.address.detail);
        $('#formprovUpdate').val(data.address.province);
        $('#Tanggal').val(data.tanggal);
        //-----
        // cari id provinsi
        var idProvUpdate = '';
        for (var x =0;x<($("#formprovUpdate> option").length);x++){
            $("#formprovUpdate").prop('selectedIndex', x);            
            if ($('#formprovUpdate').val()==data.address.province) {
                idProvUpdate = x;
                console.log(data.address.province+' >>>>>>> KETEMU ID:'+idProvUpdate);
            }
        }

        // get json kota
        var urlKotaUpdate = 'https://demoapi.blood4life.id/v1/provinces/c/'+idProvUpdate;
        $.getJSON(urlKotaUpdate,function (json) {
            var dataKota = json.rajaongkir.results;
            console.log(dataKota);
            // lanjutkan...
            $.each(dataKota, function(i, item){
                var view = '<option value="'+item.type+' '+item.city_name+'">'+item.type+' '+item.city_name+'</option>'
                $('#formkotaUpdate').append(view);
            })
        }).done(function(){
                $('#formkotaUpdate').val(data.address.city);
                
            });
        //-----
        console.log(data.address.province+" - "+data.address.city);
    })
    idEdit=id;
};

$('#SubmitUpdate').on('click', function(z){
    z.preventDefault();
    console.log(idEdit);
    $.ajax({
        type : "PUT",
        url : "http://localhost:3001/v1/rumahsakit/"+idEdit,
        data : $('formUpdate').serialize(),
        success : function(data){
            console.log("berhasil di Edit");
            setTimeout(function(){
                window.location.href="http://localhost:3000";    
            },2000);
        },
        error : function(err){
            console.log('gagal di Edit');
        }
    })
})

$.getJSON('http://localhost:3001/v1/rumahsakit', function(json){
    var data = json.result;
    $.each(data, function(i, item){
        var view =
        '<tr><td>'+item.name+'</td>'+
            '<td>'+item.jenis+'</td>'+
            '<td>'+item.address.detail+'</td>'+
            '<td>'+item.address.province+'</td>'+
            '<td>'+item.address.city+'</td>'+
             '<td>'+item.tanggal+'</td>'+
            '<td><button onclick="tampilkanFormUpdate('+"'"+item._id+"'"+')">Edit</button><button onclick="deleteRs('+"'"+item._id+"'"+')">Delete</button></td></tr>'
        $('#View').append(view)
    })
})

$('#addSubmit').on('click', function (z) {
    if ($('#formSubmit').valid()==true) {
        z.preventDefault();
        $.ajax({
            type: "POST",
            url : "http://localhost:3001/v1/input",
            data : $('#formSubmit').serialize(),
            success : function(data){
                console.log('berhasil');
                console.log(data);
                $('#pesanServer').html('Selamat anda telah berhasil menambahkan satu data rumah sakit kedalam database');
                setTimeout(function(){
                    window.location.href="http://localhost:3000";    
                },2000);
            },
            error : function(err){
                console.log('gagal');
                alert('gagal input');
                setTimeout(function(){
                    window.location.href="http://localhost:3000";    
                },2000);
            }
       });
    } else {
        alert('Data harus di isi semua');
    }
});

function deleteRs(id) {
  console.log(id);
  $.ajax({
    type:"DELETE",
    url: "http://localhost:3001/v1/rumahsakit/"+id,
    success: function () {
      alert('Data Terhapus');
      setTimeout(function(){
            window.location.href="http://localhost:3000";    
        },500);
    },
    error: function () {
      console.log('Gagal dihapus');
    }
  })
}

$('li').click(function(){
    $('li').removeClass('active');
    $(this).addClass('active')
})
