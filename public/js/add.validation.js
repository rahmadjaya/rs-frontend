(function($){
    $.fn.serializeObject = function(){

        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };

        this.build = function(base, key, value){
            base[key] = value;
            return base;
        };

        this.push_counter = function(key){
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function(){

            // skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while((k = keys.pop()) !== undefined){

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if(k.match(patterns.push)){
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }

                // fixed
                else if(k.match(patterns.fixed)){
                    merge = self.build([], k, merge);
                }

                // named
                else if(k.match(patterns.named)){
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };
})(jQuery);


$('#formSubmit').validate({
   rules :{
    name : {required : true, minlength : 2},
    jenis : {required: true},
    "address[detail]" : {required: true, minlength: 10},
    "address[province]" : {required: true},
    "address[city]" : {required: true},
    tanggal : {required:true, date : true}
   },
   messages : {
    name: {required : "Nama harus diisi", minlength : "Nama harus lebih dari 2 karakter"},
    jenis : {required: "Pilih salah satu"},
    "address[detail]" : {required : "Alamat harus diisi", minlength : "Alamat harus lebih dari 10 karakter"},
    "address[province]" : {required: "Pilih salah satu"},
    "address[city]" : {required: "Pilih salah satu"},
    tanggal: {required: "Tanggal harus diisi", date : "Format harus tanggal"}
   }
});
$('.content-login').validate({
    rules : {
        username : {required: true, minlength:4},
        password : {required: true, minlength:6}
    },
    messages : {
        username : {required : "Username harus diisi", minlength : "Username harus lebih dari 4 karakter"},
        password : {required : "Password harus diisi", minlength : "Password harus lebih dari 6 karakter"}
    }
})
$('.content-register').validate({
    rules : {
        fullname : {required : true, minlength: 10},
        email    : {required: true, email:true},
        username : {required: true, minlength:4},
        password : {required: true, minlength:6}
    },
    messages : {
        fullname : {required : "Fullname harus diisi", minlength : "Fullname harus lebih dari 10 karakter"},
        email    : {required : "Email harus diisi", email : "Email harus valid"},
        username : {required : "Username harus diisi", minlength : "Username harus lebih dari 4 karakter"},
        password : {required : "Password harus diisi", minlength : "Password harus lebih dari 6 karakter"}
    }
})