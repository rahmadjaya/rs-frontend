$('.content-register').hide()
$('.log').on('click', function(){
    $('.content-login').show();
	$('.content-register').hide();
});
$('.register').on('click', function(){
    $('.content-login').hide();
	$('.content-register').show();
});

$('.on').click(function(){
    $('.on').removeClass('active');
    $(this).addClass('active')
})

$('.button-login').on('click', function(e){
	if ($('.content-login').valid()==true) {
		e.preventDefault();
		$.ajax({
			type : "POST",
			url : "http://localhost:3001/v2/login",
			data : $('.content-login').serialize(),
			success : function(data){
				localStorage.setItem('token', data.token);
				if (localStorage.token == data.token) {
					window.location.href="http://localhost:3000";
				}
			},
			error : function(err){
				console.log('login gagal');
				// window.location.href="http://localhost:3000/login";
			}
		});
	} else {
		alert('Yang anda masukan salah');
	}
})

$('.button-register').on('click', function(e){
	if ($('.content-register').valid()==true) {
		e.preventDefault();
		$.ajax({
			type : "POST",
			url : "http://localhost:3001/v2/register",
			data : $('.content-register').serialize(),
			success : function(data){
				console.log("berhasil register");
				// window.location.href="http://localhost:3000/login";
			},
			error : function(err){
				console.log('gagal register');
			}
		});
	} else {
		alert('Yang anda masukan salah');
	}
})

function cekHome(){
	token = localStorage.getItem('token');
	if (token) {
		console.log('di home')
		window.location.href= '/'
	} else{
		console.log('di login')
	}
}
cekHome();